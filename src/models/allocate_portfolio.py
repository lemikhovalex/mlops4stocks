# create_pct_change
import json
import logging
import sys

import click
import pandas as pd
import pypfopt  # type:ignore
from pypfopt import DiscreteAllocation

TOTAL_PORTFOLIO_VALUE = 1_000_000


def get_last_prices(tickers_in_model):
    data = pd.read_csv("data/interim/filtered_data.csv", index_col="Date")
    return data[tickers_in_model].iloc[-1]


@click.command()
@click.argument("input_filepath", type=click.Path())
@click.argument("output_filepath", type=click.Path())
def main(input_filepath, output_filepath):
    logger = logging.getLogger(logger_name)
    logger.info(f"Get weights_model from {input_filepath}")

    with open(input_filepath, "r") as f:
        weights = json.load(f)

    # Get lastest prices for model tickers for allcocation
    tickers_in_model = weights.keys()
    latest_prices = get_last_prices(tickers_in_model)

    # Allocate money to tickers
    da = DiscreteAllocation(
        weights,
        latest_prices,
        total_portfolio_value=TOTAL_PORTFOLIO_VALUE,
        short_ratio=0.3,
    )
    alloc, leftover = da.greedy_portfolio()

    with open(output_filepath, "w") as f:
        json.dump([alloc, leftover], f)
    logger.info(f"Store allocation portolio to {output_filepath}")


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    logger_name = sys.argv[0]
    logger = logging.getLogger(logger_name)

    main()
