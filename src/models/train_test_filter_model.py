# create_pct_change
import itertools
import json
import logging
import os
import pickle
import sys
import time

import click
import mlflow  # type:ignore
import pandas as pd
import yaml
from dotenv import load_dotenv
from filter_model import RootModel  # type:ignore

load_dotenv()
MLFLOW_TRACKING_URI = str(os.getenv("MLFLOW_TRACKING_URI"))

mlflow.set_tracking_uri(MLFLOW_TRACKING_URI)
mlflow.set_experiment("test_cloud_1")


@click.command()
@click.argument("input_filepath_train", type=click.Path())
@click.argument("input_filepath_test", type=click.Path())
@click.argument("output_filepath_train_features", type=click.Path())
@click.argument("output_filepath_test_features", type=click.Path())
@click.argument("output_filepath_model", type=click.Path())
def main(
    input_filepath_train,
    input_filepath_test,
    output_filepath_train_features,
    output_filepath_test_features,
    output_filepath_model,
):
    logger.info(
        f"Get train data from {input_filepath_train}, test data from {input_filepath_test}"
    )
    start_time = time.time()

    params = yaml.safe_load(open("params.yaml"))["train_test_filter_model"]
    all_combinations = get_all_combinations(params)

    with mlflow.start_run():
        mlflow.set_tag("file", params.get("name", "None"))
        mlflow.log_artifact("src/models/train_test_filter_model.py")
        mlflow.log_artifact("src/models/filter_model.py")
        mlflow.log_artifact("params.yaml")
        # input_filepath for train data = "data/interim/train_data.csv"
        train_data = pd.read_csv(input_filepath_train, index_col="Date")
        # input_filepath test = "data/interim/test_data.csv"
        test_data = pd.read_csv(input_filepath_test, index_col="Date")

        for n_exp, exp in enumerate(all_combinations, start=1):
            train_test_model(
                train_data,
                test_data,
                output_filepath_train_features,
                output_filepath_test_features,
                output_filepath_model,
                trust_model_by=exp["trust_model_by"],
                cutoff=exp["cutoff"],
                n_exp=n_exp,
            )
    end_time = time.time()
    logger.info(f"Training models elapsed: {(end_time - start_time):.2f} seconds ")


def train_test_model(
    train_data,
    test_data,
    output_filepath_train_features,
    output_filepath_test_features,
    output_filepath_model,
    trust_model_by,
    cutoff,
    n_exp,
):
    with mlflow.start_run(run_name="run_" + str(n_exp), nested=True):
        # init model
        if trust_model_by not in ["all", "precision", "accuracy"]:
            raise Exception(f"trust_model_by {trust_model_by} unknown")
        if (cutoff < 0) | (cutoff > 1):
            raise Exception(f"cutoff {trust_model_by} is not in range [0,1]")

        rm = RootModel(
            trust_model_by=trust_model_by,  # "all", "precision", "accuracy"
            cutoff=cutoff,  # from 0 to 1
        )

        # transform data and train model
        train_data_tr = rm.transform(train_data)

        rm.fit(train_data_tr)

        # Transform data for test
        test_data_tr = rm.transform(test_data)

        # predict on test data for create metrics
        pred = rm.predict(test_data_tr)
        # Convert the dictionary to JSON
        pred_json = json.dumps(pred)

        mlflow.log_param("trust_model_by", rm.trust_model_by)
        mlflow.log_param("cutoff", rm.cutoff)
        mlflow.log_metric("n_tickers", len(rm.filter))
        mlflow.log_metric("n_growth", len([v for v in pred.values() if v == 1]))
        mlflow.log_metric("accuracy", rm.accuracy())
        mlflow.log_metric("precision", rm.precision())
        # Log the JSON string to MLflow
        mlflow.log_param("pred_tickers", pred_json)

        with open(output_filepath_train_features, "wb") as handle:
            pickle.dump(train_data_tr, handle, protocol=pickle.HIGHEST_PROTOCOL)

        with open(output_filepath_test_features, "wb") as handle:
            pickle.dump(test_data_tr, handle, protocol=pickle.HIGHEST_PROTOCOL)

        with open(output_filepath_model, "wb") as handle:
            pickle.dump(rm, handle, protocol=pickle.HIGHEST_PROTOCOL)

        mlflow.pyfunc.log_model(
            artifact_path="own_model",
            python_model=rm,
            code_path=["src/models/filter_model.py"],  #
        )

        logger.info(
            f"Store \
            features test data to {output_filepath_test_features}, \
            filter model with metrics to {output_filepath_model} "
        )


def get_all_combinations(params_raw: dict) -> list:
    params = {k: v for k, v in params_raw.items() if k not in ["name"]}
    keys, values = list(zip(*params.items()))
    return [dict(zip(keys, v)) for v in itertools.product(*values)]


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    logger_name = sys.argv[0]
    logger = logging.getLogger(logger_name)

    main()
