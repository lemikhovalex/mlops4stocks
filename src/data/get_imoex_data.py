import logging
import subprocess

start = "2020-01-01"
end = "2023-03-31"
path = "data/raw/imoex_data.csv"
n_sessions = "1"
delay = "0.1"


# For debug purpose
# tickers = "ALRS GAZP GMKN LKOH MGNT MTSS NVTK PHOR PLZL ROSN SBER SNGS TATN TCSG YNDX"
# tickers = "AFKS AFLT AGRO ALRS CBOM CHMF ENPG FIVE FIXP GAZP GLTR GMKN HYDR IRAO LKOH"
# tickers = "MAGN MGNT MOEX MTSS NLMK NVTk OZON PHOR PIKK PLZL POLY ROSN RTKM RUAL SBER SBERP SNGS"
# tickers = "AFKS AFLT AGRO ALRS CBOM CHMF ENPG FIVE FIXP GAZP GLTR GMKN HYDR IRAO LKOH MAGN MGNT MOEX MTSS NLMK NVTk OZON PHOR PIKK PLZL POLY ROSN RTKM RUAL SBER SBERP SNGS SNGSP TATN TATNP TCSG TRNFP VKCO VTBR YNDX"


def main():
    """Get test data from MOEX API for list of tickers"""

    with open("data/external/imoex_tickers") as f:
        txt = f.read().splitlines()
        tickers = " ".join(txt)

    # print(type(txt), txt)
    subprocess.run(
        [
            "python",
            "src/data/get_data.py",
            tickers,
            path,
            "--start",
            start,
            "--end",
            end,
            "--n_sessions",
            n_sessions,
            "--delay",
            delay,
        ]
    )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    main()
