#!/usr/bin/env bash

/usr/bin/mc config host add myminio http://minio:$MINIO_PORT $MINIO_ROOT_USER $MINIO_ROOT_PASSWORD; \
    /usr/bin/mc mb myminio/mlflow; \
    /usr/bin/mc mb myminio/mlflow; \
    /usr/bin/mc mb myminio/dvc; \
exit 0